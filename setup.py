from setuptools import setup, find_packages

setup(
    name="gnome-shell-extension-installer",
    version="0.1",
    packages=find_packages(),
    install_requires=["docopt", "requests"],
    python_requires=">=3.7",
    entry_points="""
        [console_scripts]
        gnome-shell-extension-installer=gnome_shell_extension_installer:main
    """,
)
