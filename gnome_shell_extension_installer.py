"""Gnome-Shell-Extension-Installer

Usage:
  gnome-shell-extension-installer debug
  gnome-shell-extension-installer -h | --help
  gnome-shell-extension-installer --version
  gnome-shell-extension-installer get-shell-version
  gnome-shell-extension-installer search EXTENSION_NAME [-s SORT_BY | --sort SORT_BY] [debug]
  gnome-shell-extension-installer download EXTENSION_ID [debug]
  gnome-shell-extension-installer install EXTENSION_ID [debug]
  gnome-shell-extension-installer install-multiple EXTENSION_IDS [debug]
  gnome-shell-extension-installer update [debug]
  gnome-shell-extension-installer restart-shell [debug]



Options:
  -h, --help                     Show this screen
  --version                      Show version
  -s SORT_BY, --sort SORT_BY     Sort search output by SORT_BY where SORT_BY can be name, recent, downloads, or popularity. Default: name
  -v                             A flag to set verbosity to gather more information/logging, default False
  --vv                           A flag to set double verbosity to gather more information/logging, default False

"""

from docopt import docopt
import requests
from typing import Mapping
from pathlib import Path
import subprocess
from collections import namedtuple
from pprint import pprint
from typing import NamedTuple


def main():
    arguments = docopt(__doc__, version="Gnome-Shell-Extension-Installer 0.1")
    extensions = []

    if arguments.get("debug"):
        print(arguments)
    elif arguments.get("search"):
        if arguments.get("--sort"):
            extensions = search_extension(
                arguments.get("EXTENSION_NAME"), True, arguments.get("--sort")
            )
        else:
            extensions = search_extension(arguments.get("EXTENSION_NAME"))
    elif arguments.get("download"):
        download_extension(get_extension_info(arguments.get("EXTENSION_ID")))
    elif arguments.get("install"):
        download_unzip_install(get_extension_info(arguments.get("EXTENSION_ID")))
    elif arguments.get("install-multiple"):
        # download_unzip_install_multiple(extension_named_tuple)
        for extension_id in [item for item in arguments.get("EXTENSION_IDS").split(",")]:
            extensions.append(get_extension_info(extension_id))
        download_unzip_install_multiple(extensions)
    elif arguments.get("get-shell-version"):
        print(get_gnome_shell_version())
    elif arguments.get("update"):
        pass
    elif arguments.get("restart-shell"):
        restart_gnome_shell()


def download_unzip_install_multiple(extensions: list) -> None:
    skipped = 0
    for extension in extensions:
        uuid = extension.uuid

        if extension_exists(uuid):
            print(f"Extension {uuid} already exists, skipping...")
            skipped += 1
            continue

        download_extension(extension)
        create_extension_dir(uuid)
        unzip_extension(uuid)
        cleanup_extension_zip(uuid)

    if not skipped == len(extensions):
        restart_gnome_shell()

        for extension in extensions:
            install_extension(extension.uuid)




def get_extension_info(extension_id: str) -> Mapping:
    request = requests.get(
        f"https://extensions.gnome.org/extension-info/?pk={extension_id}"
    )
    request.raise_for_status()
    return extract_extensions_info([request.json()])[0]


def search_extension(extension_name: str, sort_=False, SORT_BY="") -> list:
    page = 1
    extension_query = None
    extensions = []
    if sort_:
        extension_query = f"https://extensions.gnome.org/extension-query/?sort={SORT_BY}&search={extension_name}&page={page}"
    else:
        extension_query = f"https://extensions.gnome.org/extension-query/?search={extension_name}&page={page}"

    request = requests.get(f"{extension_query}")
    request.raise_for_status()
    extension_info = request.json()
    if extension_info["extensions"]:
        extensions = extract_extensions_info(extension_info["extensions"])
        print_extension_info(extensions)

    return extensions


def extract_extensions_info(extensions_info: list) -> Mapping:
    Extension = namedtuple(
        "Extension",
        [
            "creator",
            "creator_url",
            "description",
            "icon",
            "link",
            "name",
            "pk",
            "screenshot",
            "shell_version_map",
            "uuid",
        ],
    )
    extensions = []
    for extension in extensions_info:
        extensions.append(
            Extension(
                creator=extension.get("creator"),
                creator_url=extension.get("creator_url"),
                description=extension.get("description"),
                icon=extension.get("icon"),
                link=extension.get("link"),
                name=extension.get("name"),
                pk=extension.get("pk"),
                screenshot=extension.get("screenshot"),
                shell_version_map=extension.get("shell_version_map"),
                uuid=extension.get("uuid"),
            )
        )

    return extensions


def print_extension_info(extensions: list) -> None:
    print("Extension Information: ")
    for extension in extensions:
        # pprint(extension)
        print(
            f"Name: {extension.name}\nCreator: {extension.creator}\nLink: {extension.link}\nPK: {extension.pk}\nUUID: {extension.uuid}\nDescription: {extension.description}"
        )
        print(
            "\n---------------------------------------------------------------------------------------------\n"
        )


def download_unzip_install(extension: NamedTuple) -> None:
    uuid = extension.uuid

    if extension_exists(uuid):
        print("Extension already exists!")
        return

    download_extension(extension)
    create_extension_dir(uuid)
    unzip_extension(uuid)
    cleanup_extension_zip(uuid)
    restart_gnome_shell()
    install_extension(uuid)


def restart_gnome_shell() -> None:
    print("Restarting Gnome Shell...")
    subprocess.run(
        ["killall", "-3", "gnome-shell"], stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )


def enable_extension(uuid: str, has_gnome_extensions: bool) -> None:
    if has_gnome_extensions:
        subprocess.run([f"gnome-extensions", "enable", f"{uuid}"])
    elif not has_gnome_extensions:
        subprocess.run(["gnome-shell-extension-tool", f"--enable-extension={uuid}"])
    else:
        print(
            "Cannot enable extension, neither gnome-extensions or gnome-shell-extension-tool are present on the system..."
        )


def install_extension(
    uuid: str, path=Path().home() / ".local/share/gnome-shell/extensions"
) -> None:
    gnome_extensions_check = subprocess.run(
        ["gnome-extensions", "--version"], stdout=subprocess.PIPE
    )
    if gnome_extensions_check.returncode == 0:
        enable_extension(uuid, True)
    else:
        enable_extension(uuid, False)


def unzip_extension(
    uuid: str, path=Path().home() / ".local/share/gnome-shell/extensions"
) -> None:
    subprocess.run(
        ["unzip", f"./{uuid}.zip", "-d", f"{path}/{uuid}"], stdout=subprocess.PIPE
    )


def create_extension_dir(
    uuid: str, path=Path().home() / ".local/share/gnome-shell/extensions"
) -> None:
    subprocess.run(["mkdir", "-p", f"{path}/{uuid}"], stdout=subprocess.PIPE)


def cleanup_extension_zip(uuid: str) -> None:
    subprocess.run(["rm", f"./{uuid}.zip"])


def extension_exists(
    uuid: str, path=Path().home() / ".local/share/gnome-shell/extensions"
) -> bool:
    for p in path.glob("*"):
        if uuid in str(p):
            return True

    return False


def download_extension(extension: NamedTuple, path=".") -> None:
    version_tag = None
    current_shell_version = get_gnome_shell_version()
    uuid = extension.uuid

    for version, value in extension.shell_version_map.items():
        if version in current_shell_version:
            version_tag = value["pk"]

    subprocess.run(
        [
            "curl",
            "-Lsf",
            f"https://extensions.gnome.org/download-extension/{uuid}.shell-extension.zip?version_tag={version_tag}",
            "-o",
            f"./{uuid}.zip",
        ],
        stdout=subprocess.PIPE,
    )


def get_gnome_shell_version() -> str:
    version = subprocess.run(["gnome-shell", "--version"], stdout=subprocess.PIPE)
    return version.stdout.decode("UTF-8").split(" ")[2]


if __name__ == "__main__":
    main()
