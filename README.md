# Gnome Shell Extension Installer

## Requirements

Python >= 3.7 - should be installed on any Linux distro >= 20.04
Pip for Python 3 - `sudo apt install python3-pip`


## Installation

`git clone https://gitlab.com/NottinghamNerds/gnome-shell-extension-installer/`

`cd gnome-shell-extension-installer`

`pip3 install -e .`


## Usage:

  gnome-shell-extension-installer debug

  gnome-shell-extension-installer -h | --help

  gnome-shell-extension-installer --version

  gnome-shell-extension-installer get-shell-version

  gnome-shell-extension-installer search EXTENSION_NAME [-s SORT_BY | --sort SORT_BY] [debug]

  gnome-shell-extension-installer download EXTENSION_ID [debug]

  gnome-shell-extension-installer install EXTENSION_ID [debug]

  gnome-shell-extension-installer disable EXTENSION_ID [debug]

  gnome-shell-extension-installer enable EXTENSION_ID [debug]

  gnome-shell-extension-installer update [debug]

  gnome-shell-extension-installer restart-shell [debug]
  
## Options:

  -h, --help                     Show this screen
  
  --version                      Show version

  -s SORT_BY, --sort SORT_BY     Sort search output by SORT_BY where SORT_BY can be name, recent, downloads, or popularity. Default: name

  -v                             A flag to set verbosity to gather more information/logging, default False
  
  --vv                           A flag to set double verbosity to gather more information/logging, default False
